# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:5
#: 36c10d1d8bcc459dbea7594f1c3ecbc9
msgid "IMPOSTAZIONI MENÙ UTENTI"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:24
#: a217ab007df84030a65838dd100ed9f5
msgid ".. image:: _static/it/impostazioni_applicazione_utenti.png"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:24
#: 24759fe7d7624865adcb89316d11eec3
msgid "Menù Utenti"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:27
#: 59f52978c2024abaa457e8634a70d13c
msgid "Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > Utenti` permette di aggiungere e togliere utenti e di definirne le abilitazioni."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:29
#: aa70d03c42124e979e80e55116eb87f6
msgid "Il pulsante |Abilita_Gestione_Utenti| permette di abilitare la gestione degli utenti;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:7
#: cd994e37cd894deaaa2634abdda077bf
msgid ".. image:: _static/impostazioni_utenti_abilita.png\n"
"   :alt: Abilita_Gestione_Utenti"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:30
#: d25cfeefd8b14313ba8f97b5a560e2b6
msgid "Il pulsante |Aggiungi_Utente| permette di aggiungere un utente;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:11
#: eab26f44bc834b79852526251f81b679
msgid ".. image:: _static/impostazioni_utenti_aggiungi.png\n"
"   :alt: Aggiungi_Utente"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:31
#: 8a1d91c55f7544449b07b334ba567bf1
msgid "Il pulsante |Elimina_Utente| permette di eliminare un utente."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:15
#: cd994e37cd894deaaa2634abdda077bf
msgid ".. image:: _static/impostazioni_utenti_elimina.png\n"
"   :alt: Elimina_Utente"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:33
#: b2673fbb94cf47d299dfb6b98b269dfb
msgid "Per ogni utente si deve definire il ruolo **Administrator** oppure **Basic User** e definire :kbd:`Nome` e :kbd:`Password`."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:37
#: acc38563ea474881aa706cd832099676
msgid "PROPRIETÀ GENERALI"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:38
#: 8a4489f4f1374542842e69506485be00
msgid "**Acconsenti disabilitazione Piani Lavoro**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:40
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:44
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:48
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:52
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:56
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:60
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:64
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:68
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:72
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:76
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:80
#: e091e476116b42f892bb21f79fd9fd04
#: c0a8a5a0fbc34f808f3da204b2c3e290
#: cd3175ee8b2042e283ce2eb62bee4d4b
#: c5531eac2639412fbd71a9e08f0012ee
#: 3c2f9ee0da7b47569796fe58551cff3a
#: ca9487b489144145b1afd0d26a58b1a8
#: 7f2f92e7550d406fa558013198728dcf
#: c9a0623257e944dd8f6a7ae2f0ebcf37
#: d1834fa4d95e4f7c911433b1b74123a4
#: 925e2cd827ea465595300ab2d3bde6c0
#: ad1b5f4e827b47c7a8ad169c76c26e1a
msgid "*Inserire descrizione*"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:42
#: d73c79fa90de41459aa13877cec1d968
msgid "**Settaggi**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:46
#: 1c4d1cc672a34188a63b97d0d9de7b12
msgid "**Livelli**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:50
#: 9ad2b4af874e4779b4efe0205bf279bb
msgid "**Modalità**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:54
#: 237d502eb3d54c8096e21556265e019a
msgid "**Posizione**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:58
#: 29f3e33d5cb245dfaf78c4885bca03cf
msgid "**Variabili e Foglio dati**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:62
#: 3d7a8b4dd022400789c2173429a27e24
msgid "**Allarmi**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:66
#: b08a7985e76946fba68632aed8655d19
msgid "**Copie multiple**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:70
#: 619c9e36b8b643dfa358d6ec207b2088
msgid "**Lavoro in Pausa**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:74
#: ff33b2ec05f24a3da6602513281ab0de
msgid "**Messaggio personalizzato**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_utenti.rst:78
#: 7ca1cacad85b4b28af2a269fee77ade1
msgid "**Posizione personalizzata**"
msgstr ""
