# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:5
#: 653bd63f7572493eb6d490aa3aaca378
msgid "TEST PFS"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:8
#: e630695c10cc4c59b3cd965b9d7d10c0
msgid "|danger| **Pericolo di danni a cose e/o persone**"
msgstr ""

#: <rst_prolog>:2
#: fcde0c8c0a384fbf9948cfcf3734af4b
msgid ".. image:: _static/danger.png\n"
"   :alt: danger"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:10
#: d53a7377db564ccb95c4caa2ead7900d
msgid "Questo test permette di andare ad eseguire una marcatura sul materiale per identificare i parametri più idonei al materiale stesso."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:12
#: b8b6ec4dcfa748bb9167b94006fb82f3
msgid "La compilazione dei parametri del test richiede una buona conoscenza delle modalità di utilizzo del programma e familiarità con i parametri macchina."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:14
#: cacef86b3ca1411992f052bbcdf3c191
msgid "L'impostazione dei parametri per la marcatura è riservata ad un utente esperto."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:21
#: d7f5b1b9b1b44fccb81ebf2d4ff93e1e
msgid ".. image:: _static/it/PFS_test.png"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:21
#: 76cfb10501d94b85a6f98e6b535b4bfb
msgid "Finestra *PSF TEST*"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:25
#: f38c36d9dfee4715b4cffff65169aaae
msgid "ESECUZIONE TEST"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:26
#: 7ca232dc987c4a4e99bc911dca82da85
msgid "Il test eseguirà una serie di marcature incrociando i range di *Potenza* - *Frequenza* - *Velocità* che vogliamo verificare."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:28
#: b122ef4b98e7470ea43fe4aa650499f0
msgid "Per il test si procede decidendo quale dei 3 parametri mantenere fisso. In funzione della scelta si andranno ad impostare i limiti minimo e massimo degli altri due parametri."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:30
#: e6f8053da71b4cc5b6298d71aa3df707
msgid "Ad esempio (vedere :numref:`PFS_test`), se si desidera lasciare fissa la *Velocità* basterà selezionare *Potenza* - *Frequenza*."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:32
#: 512edd49f60e48ea861279017e7a5541
msgid "Fissato questo parametro seguire le seguenti istruzioni di compilazione del test:"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:34
#: c0b725d208da4b4192a1227e6992682a
msgid "Impostare il range (Min-Max) dei valori da testare ed il valore da assegnare al parametro fisso scelto;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:35
#: e3050b91845e4adaaa2f85e8a0ae4886
msgid "Verificare che:"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:37
#: 3be93f0f24af454fb7381eee05825c6c
msgid "la :kbd:`Forma d'Onda` sia quella che desideriamo testare;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:38
#: ddf8fae7bdcc43b3af6bf18613f352ec
msgid "la :kbd:`Spaziatura Riempimento` sia corretta per il test;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:39
#: 702800f2e5ea4f3ea5c276591c8eb7bc
msgid "lo :kbd:`Zoom [%]` permetta di avere una dimensione del test adeguata all'area da marcare."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:41
#: 6ba5628e7d594fe5817c51c845a4c31c
msgid "Selezionare tra le ulteriori opzioni presenti quelle necessarie al test:"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:43
#: 5cd0c7b1385b4e2689557c7b8c66e90d
msgid ":kbd:`Usa Livello Corrente` *inserire descrizione*;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:44
#: cb2e45add302430e9d30b1040af449c2
msgid ":kbd:`Riempimento bidirezionale` *inserire descrizione*;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:45
#: e1856b653832479893b7718211f7b880
msgid ":kbd:`Linea Sottile` *inserire descrizione*;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:46
#: b8d3561426224084bf9dbc662ec0124a
msgid ":kbd:`Non muovere testa` *inserire descrizione*."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:48
#: a39202cee45b46edbd93b48361d3e707
msgid "Premere :kbd:`AVVIA ROSSO`;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:49
#: 67433143ed224550a43e5ab013478f6d
msgid "Posizionare il materiale all'interno della camera, centrando il rosso sull'area interessata alla marcatura;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:50
#: 8a82dbc1257b405eaea0c3cc97a68300
msgid "Chiudere il portellone del laser;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:51
#: c8a327979fe54dfd8c5dcf0c5a718127
msgid "Premere :kbd:`AVVIA LASER`"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:52
#: 8ea68b889b3b4edc941de70e64dbc7cf
msgid ":kbd:`Aria` *inserire descrizione*;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:53
#: 63819d2df6f249f3967590bbe8b072b8
msgid ":kbd:`STOP` permette di arrestare il test."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:57
#: 308d7bfacaf74371a2e1d6cf5d726c36
msgid "RISULTATO TEST"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:58
#: 034274ffce2649109f45bcac7cfebaf0
msgid "Una volta eseguito il test verificare se uno dei quadratini marcati soddisfa le esigenze di marcatura."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:60
#: 486c2e7648d14fa2bd3ade3390ba2ada
msgid "In caso di risposta affermativa, per poter utilizzare i parametri trovati:"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:62
#: 0d83604a16c44f68adb7c55f8807b3b2
msgid "Chiudere il :guilabel:`PFS TEST`;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:63
#: 30b99d96416546e9a13d0036113107cf
msgid "Aprire un File Livelli nuovo;"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\test_pfs.rst:64
#: b29fc1236f7b494a947e8a62aca166d3
msgid "Copiare i valori indicati nella piastrina del test nel colore del Livello scelto."
msgstr ""
