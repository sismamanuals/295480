# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:3
#: 64e8f0c1090f4c41840f388555473f22
msgid "IMPOSTAZIONI STRUTTURA PROGETTO"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:4
#: 9c258f405ff24f5f8ec2c051ab154289
msgid "Ogni elemento presente del progetto possiede delle proprietà."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:6
#: fcf12a6e9630403aa651458a07acaf7e
msgid "Il progetto è un contenitore di file e impostazioni, che consentono di produrre la marcatura attesa. La struttura del progetto è costituita da **Piani di lavoro**."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:8
#: cbd0bd1e31a040baaa7aac6ae09a4333
msgid "Il piano di lavoro è un sotto-elemento che racchiude tutte le informazioni necessarie per effettuare una marcatura completa. In questa scheda vedremo come impostare un piano di lavoro che preveda la marcatura di un file in piano."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:13
#: 6ea392ce418f48e08fea309e3f196658
msgid "PROPRIETÀ WORKING PLANE"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:20
#: 4fd540af4b8e4aba8850d5ee8fe5d077
msgid ".. image:: _static/it/impostazioni_struttura_progetto.png"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:20
#: 9090ce8beefc474d855ad216659ddd83
msgid "Piani di lavoro"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:23
#: 2b26f52331f84bbd8d56c6737a677e62
msgid "Nella finestra :guilabel:`Struttura Progetto` cliccare due volte su :kbd:`Working Plane 0` e digitare il nome del piano di lavoro (ad esempio: Piano 1) oppure lasciare il nome di default (:kbd:`Working Plane 0`)."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:25
#: 1edc13c8af494ac5b5ea269f87ca35c3
msgid "Se si aggiungono piani di lavoro questi avranno la dicitura di default :kbd:`New Plane`."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:29
#: 1ec5742dc8314b37858518264b59da7b
msgid "PROPRIETÀ CONFIGURAZIONE"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:36
#: 2811504c0ebc4d6cb1391a912fbd70bc
msgid ".. image:: _static/it/impostazioni_struttura_progetto_configurazione.png"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:36
#: 53619c5afa9444e1b45c1668e9695498
msgid "Finestra *CONFIGURAZIONE*"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:38
#: 8e9e88190a8b49a484964559202d9ce0
msgid "**Modalità**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:40
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:44
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:48
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:53
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:61
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:65
#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:69
#: 0a1c4e06036744aebd0d8fba32af1647
#: d13d01d00b4d452d8d3dfff37dda94f9
#: 514e091f83504534a7919be7b2e37241
#: 1c182e86ac79423bbe081a67e45d4eaa
#: 4d493578332b421b9536b6346c2db1c8
#: 181e34d5fa3d4267b87376a3b60ccad0
#: 7ace3f71196f4ec7ac583506f2183c64
msgid "*Inserire descrizione*"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:42
#: 74ef9058e0144f209d4fa571a92f869c
msgid "**Origine**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:46
#: 5f82b01190bb4c239abb4d73e162cd6f
msgid "**X [mm]**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:51
#: 54c23d6f52574e4aa404a5f0f0217ed6
msgid "**Y [mm]**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:55
#: 6d884f34d83343dcb19a715d63aebbd9
msgid "**Z [mm]**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:57
#: 1f5bcc3645e446b39b8b132f06266062
msgid "Altezza del piano da marcare. Dopo aver definito il parametro premere :kbd:`GO`. L'asse Z si porterà all'altezza impostata."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:59
#: 24a05ef406f349f18dbb75bc9e41f758
msgid "**R [°]**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:63
#: 127cb3d912684ad4b27b522baa7b2cdc
msgid "**Multi**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:67
#: ec535a7a70cd4e66a83f5d654159bb99
msgid "**Inizio/Fine**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:71
#: 47ad1768695b4a609383809cfd5dd57c
msgid "**Icone di selezione**"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:73
#: 8e7627607fac4061857bd635c17f4aec
msgid ":kbd:`Pausa` (*Inserire descrizione*);"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:74
#: 4f2427a572704aee933ad9ae8779c9a1
msgid ":kbd:`Rosso` (*Inserire descrizione*);"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:75
#: 69f2e4df7a0d45d8a1119ed85ca5e7fd
msgid ":kbd:`???` (*Inserire descrizione*);"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:76
#: e8eabda1b08740f898966033963ea385
msgid ":kbd:`Autofocus` (*Inserire descrizione*);"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:77
#: d3576fcd4a79432899e11739804eecb4
msgid ":kbd:`Modifica impostazioni autofocus` (*Inserire descrizione*)."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:81
#: ce388ddafd35406c9dc69da8beedb800
msgid "PROPRIETÀ FILES"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:88
#: eb3081165776489ba0dfbb570f4a1ba4
msgid ".. image:: _static/it/impostazioni_struttura_progetto_files.png"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:88
#: 7b0968d6cdae424f98572eb06d6a3a45
msgid "Finestra *FILES*"
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:91
#: eef791b098e24aecb3b3bf5e17d57505
msgid "In questa finestra si può agire su alcune caratteristiche del file selezionato."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:93
#: 2ae79badef2245d1a51b0e7692a7e1d8
msgid "Aprendo i menù a tendina è possibile cambiare il colore di **Bordo** e **Pieno** del file, così come disattivare la visualizzazione dell'oggetto oppure bloccarne la selezione."
msgstr ""

#: Z:\MANUALI WIP\295480\295480\source\impostazioni_struttura_progetto.rst:97
#: 8a13b1adfbf24f16af4e7de1ea97b552
msgid "|notice| Salvare il nuovo progetto creato, così da poterlo richiamare in caso di necessità. Per fare questo riferirsi a :numref:`salvataggio_impostazioni` (:ref:`salvataggio_impostazioni`)."
msgstr ""

#: <rst_prolog>:14
#: 928772798160487c8d7ec05b5c9dd4ea
msgid ".. image:: _static/notice.png\n"
"   :alt: notice"
msgstr ""
