# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software
# Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: 393284cd363b40f1a402384188393521
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:5
msgid "IMPOSTAZIONI DB COMMESSE"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:8
#: c86a2788983043f4aaa377db99c49d0a
msgid "Funzionalità di connessione database PRISMA"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:9
#: b611ce4b61d0488d8bf64ab02d4cdcc2
msgid ""
"Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > DB Commesse` "
"permette di connettersi a un database esterno e caricare automaticamente "
"i progetti relativi a un risultato della query."
msgstr ""

#: 032036c535d84d40a19e64b4ab4a045e
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:12
msgid "PROPRIETÀ DB LAVORI"
msgstr ""

#: 5dba31b8f2e248338eaabd862e0a4d24
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:13
msgid ""
"Attivare l'opzione spuntando la casella indicata al punto 1 della "
":numref:`impostazioni_applicazione_DBcommesse_abilitazione`;"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:14
#: f77e5e1499c74b63b15dc2830f7a6643
msgid ""
"Selezionare la cartella di progetto selezionando il pulsante indicato al "
"punto 2 della "
":numref:`impostazioni_applicazione_DBcommesse_abilitazione`;"
msgstr ""

#: 6fb0d211208f48088f15cfa5398239f7
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:15
msgid ""
"Impostare la stringa di connessione del database nella finestra che "
"apparirà (vedere "
":numref:`impostazioni_applicazione_DBcommesse_connessioneDB`)."
msgstr ""

#: 13c6225b33b04d288d9a0f8c73cb3a59
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:22
msgid ""
".. image:: "
"_static/it/impostazioni_applicazione_DBcommesse_abilitazione.png"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:22
#: daec943e0df84fb5adb0cf94d4287fbe
msgid "Schermata DB Lavori"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:29
#: f8467f88efc04cde91a7eb0d03658bb5
msgid ""
".. image:: "
"_static/it/impostazioni_applicazione_DBcommesse_connessioneDB.png"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:29
#: b17c09c53f8e4c83b4f8df80034af62a
msgid "Schermata Connessione DB"
msgstr ""

#: 8f8501fa8e4849c493c4c85cda42cd3d
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:31
msgid ""
"I parametri della schermata in "
":numref:`impostazioni_applicazione_DBcommesse_connessioneDB` sono:"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:33
#: b59ae18b012042e5b990e668c1aef6f0
msgid ":kbd:`Nome Data source` nome del server dove il database è allocato;"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:34
#: a05f38352f6242ad875d988d72880417
msgid ":kbd:`Nome Database` è il nome del database al quale connettersi;"
msgstr ""

#: 71f9bf7cdfcb4970b52ae6d4cec4fa7d
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:35
msgid ""
":kbd:`User ID` e :kbd:`Password` sono le credenziali con le quali "
"accedere al database;"
msgstr ""

#: 32b22a25dd3f4b088038290856b69a2c
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:36
msgid ":kbd:`DB Remoto` indica se il database è locale o meno."
msgstr ""

#: 6a08f327fe0c43aabc705fb9e5a6b827
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:38
msgid ""
"La corretta impostazione dei parametri può essere verificata tramite il "
"tasto :kbd:`Test`. Il colore dell'icona indica il risultato della "
"verifica:"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:40
#: ba63eef436854da899b14a6bb1b0304c
msgid "*verde* significa che la connessione è andata a buon fine;"
msgstr ""

#: 30bb63b2174b44fa92d428e170ebfc09
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:41
msgid "*rossa* significa che la connessione non è andata a buon fine."
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:43
#: b12fc26e53664747b04ecd17329b55ac
msgid ""
"Nella casella :guilabel:`Select Query`, scrivere la query per ottenere "
"risultati utilizzando la lingua standard di SQL Server."
msgstr ""

#: 2b80e1737bcb4867b0728fbf2919d451
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:45
msgid ""
"Questa query deve avere un parametro chiamato **@PARAM_ID** che verrà "
"sostituito dal codice letto dalla finestra principale :guilabel:`Carica "
"progetto da DB`."
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:47
#: cbaafacaaffb4acdace236229c24e184
msgid ""
"**Code Reader Mask** è una maschera che può filtrare il codice letto, "
"usando alcuni caratteri speciali:"
msgstr ""

#: 94890a37f40f438a8c1f5edce243cad8
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:49
msgid "Usa il simbolo ``#`` per saltare un carattere;"
msgstr ""

#: 1de00320b17f4c1897bb88e48a0c9d3f
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:50
msgid "Usa ``*`` per leggere il carattere;"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:51
#: be4d1741b3ff4f5e80d3846e73309def
msgid "Usa ``?`` per impostare l'ultimo carattere leggibile."
msgstr ""

#: 4f4e22df6d8a498aacc5a8beedc2cd54
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:53
msgid ""
"Se, per esempio, il codice è ``AQ7P1234DFTY9745FTCV`` e la maschera è "
"``####************?`` allora il risultato sarà ``1234DFTY9745``. Questo "
"perché i primi 4 caratteri vengono saltati, così come quelli successivi "
"al ``?``."
msgstr ""

#: 4bbf60cb9b73405dbbede018d63c298b
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:55
msgid ""
"Il pulsante **Test Query** consente di verificare se la query scritta "
"restituisce alcuni risultati. In caso il controllo sia negativo "
"notificherà un errore. La tabella degli elementi della query consente di "
"abbinare le proprietà del progetto Prisma ai campi DB caricati dai "
"risultati della query."
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:58
#: eefc6a04edc14a6b85c794790b0ae281
msgid "Abbinare:"
msgstr ""

#: 429524ec0a614eaebafab1fd6cf042fa
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:60
msgid ""
"il **Tipo di Progetto** al campo della query del risultato che "
"rappresenta il valore del nome del progetto;"
msgstr ""

#: 3928b6fec83143cfb070770c53966905
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:61
msgid ""
"il **Tipo di Livello** con il campo che rappresenta il nome del file del "
"livello;"
msgstr ""

#: 7a434b6906fe4ccaa119089d74cec3c2
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:62
msgid ""
"il **Tipo di Copie** con il numero di copie archiviato che rappresenta il"
" numero di copie di lavoro;"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:63
#: ee9471c8c2d24994a7ae08d8211d9bcf
msgid ""
"un elenco di **Variabili** con campi di query correlati da aggiornare "
"quando il progetto viene caricato."
msgstr ""

#: 8068f70e60e94ffb9cca24d01b4aa9fa
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:65
msgid ""
"Al posto del nome della variabile è possibile impostare una funzione "
"speciale per abilitare o disabilitare un piano di lavoro specifico o "
"modificarne le coordinate. Sintassi della funzione speciale: ``WPnF`` "
"dove:"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:68
#: d3903de479a049a6a572c84a17dd497a
msgid "``WP``: richiama la funzionalità"
msgstr ""

#: 3601650ed44b4c5d9d78f14c20be69af
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:69
msgid "``n``: indice del piano di lavoro"
msgstr ""

#: 4071074d671249398748cc8c9a9bd0fb
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:70
msgid "``F``: funzione da applicare"
msgstr ""

#: 7dd515add1e14345a712894fbac05215
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:72
msgid "Tabella delle funzioni"
msgstr ""

#: 50e4bac642fb4c18928ed6264c6b8df3
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "Funzione F"
msgstr ""

#: 3e807a4ebce742c6a8157a5c025b22c4
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "Descrizione"
msgstr ""

#: 0dbff5367e034dfca9fd3882b70efbca
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "Valore DB"
msgstr ""

#: 1353cebbedc64951adc9916efbe9b2e0
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "E"
msgstr ""

#: 60523da4d33a4c07824e57e7e41aa74d
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "abilitato"
msgstr ""

#: 7527e1e430ea481f90811f298a646e0e
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "-1 not used, 0 disabled, 1 enabled"
msgstr ""

#: 092281ea7bcf446688235fe5da1f4b17
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "X"
msgstr ""

#: 7c48320cf5ff47bebfdb75622797cc22
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "coordinata X"
msgstr ""

#: 3395a31481c44c709edb85e74452b963 476d00e3df3d4b82abaf694f67e517c2
#: 4f6ff57bc84f470c8fc77f5a31de7036
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "Value, 1000000 do not move"
msgstr ""

#: 1478cba544504eb3819eaab54f71d0b9
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "Y"
msgstr ""

#: 0538ecda4cf649db9e20ae3c6a4466e5
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "coordinata Y"
msgstr ""

#: 3592ea772b504052b0683301cf23553f
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "Z"
msgstr ""

#: 882fc9658fc849de8943e443b467c5d5
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:1
msgid "coordinata Z"
msgstr ""

#: 1e9b87d27deb48aea67e8b22cc66b55d
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:82
msgid "CARICAMENTO PROGETTO"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:83
#: d5e12a14a5b4403cafb9429706fb7878
msgid ""
"Per caricare un progetto cliccare sull'icona in "
":numref:`caricamento_progetto_DB`"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:90
#: b75752c2135f4c54a88f3a2867a4352f
msgid ".. image:: _static/caricamento_progetto_DB.png"
msgstr ""

#: 0b279cc617a846e9989b0e0c711191c8
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:90
msgid "Schermata Caricamento Progetto DB"
msgstr ""

#: 9ba84c977ecb49dda31e075d428033cb
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:93
msgid ""
"La finestra :guilabel:`Caricamento Progetto da DB` si aprirà in attesa di"
" un codice da leggere. Se il codice si riferirà ad un risultato nel "
"database allora il software PRISMA verificherà il percorso del file di "
"progetto e del file dei layer ed aprirà il relativo progetto."
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:100
#: c61d1c2b1229482380a69a1b02a97e50
msgid ".. image:: _static/apertura_progetto_DB.png"
msgstr ""

#: 2d8d6af6aefc462380e096fe22247321
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:100
msgid "Schermata Apertura Progetto DB"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:102
#: ae18bf1de5e043bc8615881bea9c11de
msgid "Se la richiesta ritorna un errore allora una finestra informerà l'utente."
msgstr ""

#: 96da0e3eba6a49e4b2194b190f401c62
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:109
msgid ".. image:: _static/errore_apertura_progetto_DB.png"
msgstr ""

#: 82a733fccce34dbe9bcb5b87b1647e7b
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:109
msgid "Schermata Errore Apertura Progetto DB"
msgstr ""

#: 9a57938eefb04666a6af73914dd0471e
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:112
msgid "CONFIGURAZIONE DB"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:113
#: d45c1133da6a47699aa2689d1106098a
msgid "Per configurare un nuovo database:"
msgstr ""

#: 76dc9a72203b4c538e2b1d1a739551fa
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:115
msgid ""
"Connettersi al **Server Name** con autenticazione, se richiesta (vedere "
":numref:`connessione_server_name`);"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:116
#: eff12fcb83b24c8a940139e45cbea6f9
msgid ""
"Creare un nuovo database (vedere :numref:`creazione_DB_1` e "
":numref:`creazione_DB_2`);"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:117
#: e4f10b023bde4f00a8cc6b434768b774
msgid ""
"Creare una nuova tabella (vedere :numref:`creazione_tabella_1` e "
":numref:`creazione_tabella_2`);"
msgstr ""

#: 979c7678930d4e3483be4a6280c27c95
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:118
msgid ""
"Inserire o editare le righe (vedere :numref:`edit_insert_righe_1` e "
":numref:`edit_insert_righe_2`)."
msgstr ""

#: 301f3846a0a541bc9e97366df129914f
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:125
msgid ".. image:: _static/connessione_server_name.png"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:125
#: dc04842830824fba8d9ab9020cd13334
msgid "Connessione al **Server**"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:133
#: ff9fc1622a5949d8903c4a1166ce1907
msgid ".. image:: _static/creazione_DB_1.png"
msgstr ""

#: 0b540b2d84584eeeb6d79ee4f006e755
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:133
msgid "Creazione nuovo database - 1"
msgstr ""

#: 1de1388848cd414c87514e0796d74a8a
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:141
msgid ".. image:: _static/creazione_DB_2.png"
msgstr ""

#: 05c0afde94fb4c57865b60e53e446ba4
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:141
msgid "Creazione nuovo database - 2"
msgstr ""

#: 4ec68a09fc584786b986aaf309024e4c
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:149
msgid ".. image:: _static/creazione_tabella_1.png"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:149
#: fa72a58d297a4071b22b8d3dc869584e
msgid "Creazione nuova tabella - 1"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:157
#: f82c46a6677e41bc91fde6e33013d5eb
msgid ".. image:: _static/creazione_tabella_2.png"
msgstr ""

#: 6c8de531f41a498fbc449c6f071d16c0
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:157
msgid "Creazione nuova tabella - 2"
msgstr ""

#: 101a1640a27b4d1ca43cccbc0c09ee34
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:165
msgid ".. image:: _static/edit_insert_righe_1.png"
msgstr ""

#: 9f6f1a94648644d7bb24257374e6271c
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:165
msgid "Creazione/modifica righe - 1"
msgstr ""

#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:173
#: a9dc66bbcd1f499880e43cf319b1699f
msgid ".. image:: _static/edit_insert_righe_2.png"
msgstr ""

#: 1c89c337e96144bba759c92159bef47e
#: WIP/295480/295480/source/impostazioni_DB_commesse.rst:173
msgid "Creazione/modifica righe - 2"
msgstr ""

#~ msgid ""
#~ "Attivare l'opzione spuntando la casella "
#~ "indicata al punto 1 della "
#~ ":numref:`abilitazione_DB_commesse`;"
#~ msgstr ""

#~ msgid ""
#~ "Selezionare la cartella di progetto "
#~ "selezionando il pulsante indicato al "
#~ "punto 2 della :numref:`abilitazione_DB_commesse`;"
#~ msgstr ""

#~ msgid ""
#~ "Impostare la stringa di connessione del"
#~ " database nella finestra che apparirà "
#~ "(vedere :numref:`connessione_DB`)."
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_abilitazione_DB_commesse.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_connessione_DB.png"
#~ msgstr ""

#~ msgid "I parametri della schermata in :numref:`connessione_DB` sono:"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_caricamento_progetto_DB.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_apertura_progetto_DB.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_errore_apertura_progetto_DB.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_connessione_server_name.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_creazione_DB_1.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_creazione_DB_2.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_creazione_tabella_1.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_creazione_tabella_2.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_edit_insert_righe_1.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_edit_insert_righe_2.png"
#~ msgstr ""

