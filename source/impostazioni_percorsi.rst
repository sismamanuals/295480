.. _impostazioni_percorsi:

##########################
IMPOSTAZIONI MENÙ PERCORSI
##########################
Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > Percorsi` si suddivide in finestre che, a loro volta, contengono ulteriori parametri.
Di seguito descrizione dei parametri.

******************
PROPRIETÀ GENERALI
******************
**Usa Server Locale**

   Indica che SLC server si trova nello stesso computer nella quale viene eseguito Prisma. Questo è lo scenario di configurazione standard e questa opzione la si troverà sempre attiva.

**Server Locale**

   Percorso per SLC server. Il valore di default è *C:\Sisma\Prisma\SVG2RTC_SERVER\GSVG2RTC_scanlab.exe*

**Editor**

   Percorso del programma di editing. Le macchine vengono distribuite con il software di editing opensource Inkscape per cui il valore di default è *C:\Sisma\Inkscape\inkscape.exe*, tuttavia posso essere utilizzati anche altri programmi specificando modificando questo valore.

**Abilita Report** e **Cartella Report**

   Per ulteriori dettagli fare riferimento alla :numref:`report_e_raccolta_dati`.

   Attiva il sistema di reportistica le lavorazioni e stato macchina.
   In caso di utilizza della funzionalità di validazione dei codici a barre o Datamatrix, nel campo **Validazione codici** viene specificato il percorso di salvataggio delle immagini.

   Nel caso di utilizzo di un magazzino di caricamento pezzi automatico tipo WH64 è possibile abilitare un sistema di reportistica dedicato ed i file vengono salvati nel percorso specificato dal parametro **Cartella Report Magazzino**.

**Lista Progetti Recenti**

   Consente di pulire la lista dei progetti recenti visualizzati nel menù *File* dell'applicazione.

**BMES**

   Attiva la modalità di scambio informazioni con sistema gestionale BMES. Esistono 2 modalità di funzionamento:
   
   * **BMES con DB** dove un DB si preoccupa di eseguire un confronto con i lavori già eseguiti in modo da non produrre duplicati
   * **BMES senza DB** dove non c'è alcun controllo della produzione e il sistema esegue esattamente quanto indicato dal gestionale.

   I dati per le incisioni vengono passati a Prisma attraverso dei file di testo da collocare nel percorso specificato dal parametro **Cartella dei file di codici da marcare** e ad ogni lavorazione viene generato uno specifico report giornaliero nella cartella **Log codici marcati**.

**Marcatura sicura**

   *Marcatura sicura* indica una modalità di incisione per orafi che consente la riproduzione dei punzoni in modo criptato. Non vengono salvati file o mantenute informazioni su disco, le lavorazioni vengono calcolate e direttamente incise. Dopo l'inserimento del token fisico nel computer, si Attiva attraverso il parametro **Marcatura sicura**. C'è la possibilità di avere una **Richiesta password sicurezza** per l'abilitazione della funzionalità. Le anteprima di marcatura sono generate a partire da alcune immagini appositamente predisposte e messe a disposizione di Prisma attraverso il percorso specificato con il campo **Cartella Marcatura sicura**.

**Projects Manager Root Folder**

   Specifica il percorso di default dove sono caricati i progetti per i processi di caricamento automatico basati su file XML o lettura da codici. 
   Attraverso **Maschera di lettura codice progetto** possono essere definite delle regole di ricerca e caricamento del progetto stesso. La maschera viene composta da simboli che sono:
   
   * *#* salta il carattere
   * *\** leggi il carattere
   * *?* indica che è l'ultimo carattere leggibile
   * *A-Z* e *a-z* sono usati per indicare il valore della relativa variabile con nome a mono carattere

   Esempio di maschera:
   Leggendo un codice **123-ABCDEwxyz** ed elaborando con maschera **CCC#\*\*\*\*?** si ottiene:

   * Caricamento di un progetto con *nome=ABCDE*
   * Impostazione della variabile *C=123*
   * Esclusione del carattere *-*
   * Esclusione di tutti i caratteri dopo il simbolo *?*


**Auto caricamento progetto**

   **Auto caricamento progetto** abilita la funzionalità di auto-caricamento dei progetti, fare riferimento a :numref:`auto_caricamento_progetto_abilitazione`.

   Esiste la possibilità di avviare prisma nella modalità di caricamento automatico progetti attraverso l'opzione **Avvia con PRISMA**. Non può essere applicato a macchine che hanno sistemi di posizionamento.

   La gestione dei progetti tra gestionale e Prisma viene effettuata mediante un file di interscambio definito dal parametro **File di configurazione progetto**.

**Abilita Libreria Parametri**

   Questo parametro consente di abilitare la funzionalità di raccolta parametri interna a Sisma. Solo per uso interno.

**Servizio TMR**

   Il servizio TMR, attivabile con l'opzione **Abilita servizio TMR**, permette di effettuare una gestione di codici univoci di marcatura in attraverso un servizio web dedicato.
   I principali parametri di configurazione sono:
   
   * **Nome utente** per il login al servizio;
   * **Password** per il login al servizio;
   * **Indirizzo Next** indirizzo di richiesta del codice da incidere;
   * **Indirizzo Next** indirizzo di conferma del codice inciso;
   * **Auto carica prossimo codice** consente di caricare automaticamente il codice successivo dopo una incisione;
   * **Auto conferma codice dopo marcatura** conferma il codice automaticamente dopo una incisione senza errori, se non abilitato la conferma va effettuata manualmente dall'interfaccia delle variabili.

**Cartella delle immagini marcate**

   Se definito questo percorso e la macchina è provvista di un sistema di visione, dopo ogni marcatura viene salvata un'immagine della scansione dell'oggetto inciso.


.. _report_e_raccolta_dati:

**********************
REPORT E RACCOLTA DATI
**********************

ABILITAZIONE DELLA FUNZIONE
===========================
Aprire :guilabel:`Strumenti > Impostazioni > Impostazioni applicazione`.
Selezionare il menù :guilabel:`Percorsi` e attivare il flag :guilabel:`Abilita Report` e scegliere il percorso del file di configurazione del progetto (vedere :numref:`impostazioni_applicazione_percorsi_report`).

.. _impostazioni_applicazione_percorsi_report:
.. figure:: _static/impostazioni_applicazione_percorsi_report.png
   :width: 14 cm
   :align: center

   Abilitazione Report

All'interno della cartella :guilabel:`Rapporti` viene salvato un file giornaliero (*MarkingReports.csv*) con alcune informazioni relative al lavoro di marcatura, in forma differenziata a seconda della tipologia di lavorazione.

Contemporaneamente viene attivato un database (formato SQLite), su cui vengono riportate tutte le informazioni relative alla macchina e alle operazioni di lavorazione. Le stesse informazioni verranno inoltre riportate in un file di testo con estensione **.csv** formattata con ``;`` come separatore di campo e da ``|`` come separatore di informazioni interne.

Il file del database viene salvato nella directory: *C:\\Sisma\\D Database\\MarkingReportsDb.sqlite*

Il file in formato **.csv** viene salvato nella directory: *C:\\Sisma\\Reports\\MarkingReports.csv*

.. NOTE::

   |notice| Il file database creato è aperto e completamente accessibile.

STRUTTURA DEL DATABASE
======================
Il database è una tabella con diversi campi in cui vengono raccolte varie informazioni in base al tipo di evento. Il database viene aggiornato in tempo reale ogni volta che si verifica un evento. In :numref:`tabella_campi` la descrizione dei campi e in :numref:`tabella_eventi` la descrizione degli eventi.

TABELLA CAMPI
-------------

.. _tabella_campi:

.. csv-table:: Tabella delle funzioni
   :header: "Nome", "Descrizione"
   :widths: 25, 70
   :align: left

   "Date", "Data e ora dell'evento (yyyy-MM-dd HH:mm:ss)"
   "MarkingEvent", "Tipo di evento"
   "ProjectName", "Nome del progetto"
   "WorkingPlaneName", "Nome del piano di lavoro attivo"
   "MultiPositionNum", "Posizione attiva all'interno di un multi (se avviene l'evento)"
   "Level3D", "Numero di file nel caso di lavoro in 3D"
   "Lasertime", "Tempo di lavoro del singolo evento di marcatura (risoluzione 0.1 s)"
   "AlarmType", "Descrizione dell'allarme, se attivo"
   "CodeValidation", "Risultato di validazione di un codice (codice a barre o DM)"
   "PatternMatchedObjs", "Modello riconosciuto dalla funzione di corrispondenza del modello e dal numero di oggetti trovati (nome_modello ``|`` numero_oggetto)"
   "DragToolFile", "Nome del file elaborato dallo strumento *Traino lastra*"
   "Variables", "Lista delle variabili e loro valore (nome_variabile ``|`` valore)"
   "Layers", "Nome del set di parametri laser utilizzati"
   "Settings", "Nome del file dei parametri utilizzato"
   "Cycletime", "Tempo totale di lavorazione (risoluzione 0.1 s)"
   "Position", "Posizione attuale degli assi (asse ``|`` valore) (risoluzione 0.01 mm)"

TABELLA EVENTI
--------------

.. _tabella_eventi:

.. csv-table:: Tabella delle funzioni
   :header: "Nome", "Descrizione"
   :widths: 40, 70
   :align: left

   "START", "Avvio del processo di marcatura"
   "END", "Fine del processo di marcatura"
   "ALARM", "Allarme"
   "VALIDATION", "Validazione del codice a barre o del DM"
   "MARKED", "Evento di marcatura"
   "START_APPLICATION", "Avvio sessione PRISMA"
   "END_APPLICATION", "Chiusura sessione PRISMA"
   "WAREHOUSE_LOAD_PALLET", "Caricamento pallet (solo per modello WH-64)"
   "WAREHOUSE_UNLOAD_PALLET", "Scaricamento pallet (solo per modello WH-64)"

IMPOSTAZIONI REPORT
===================
Il documento di rapporto salva un file di registro che contiene tutti i passaggi di marcatura in formato **.csv**, in formato **.sqlite** oppure in formato **Microsoft SqlServer**.

Per abilitare uno oppure l'altro aprire il file *log4net.config* che si trova nella cartella :guilabel:`Risorse` della cartella principale dell'applicazione (in genere *C:\\Sisma\\Prisma\\Resources*) e seguire le istruzioni alla :numref:`report_csv`, alla :numref:`report_csv` oppure alla :numref:`report_sqlserver`, in funzione del tipo di report da impostare.

.. _report_csv:

IMPOSTAZIONI REPORT CSV
-----------------------
Per salvare un file di registro che contiene tutti i passaggi di marcatura in formato **.csv** assicurarsi che il valore ``MarkingReportCSVFileAppender`` sia attivo nella sezione **Marking Report Logger**:

.. code-block:: XML

   <!-- Marking Report Logger -->
   <logger name="SLC.Log.Report.ReportUtils" additivity="false">
      <level value="ALL" /> <appender-ref ref="MarkingReportCSVFileAppender" />
      <!--<appender-ref ref="MarkingReportSQLiteAppender" />-->
      <!--<appender-ref ref="MarkingReportSqlServerAppender" />-->
   </logger>


Il file **MarkingReports.csv** contenente i dati è salvato nella cartella :guilabel:`Rapporti` (in genere *C:\\Sisma\\Rapporti*).

.. _report_sqlite:

IMPOSTAZIONI REPORT SQLite
--------------------------
Per salvare un file di registro che contiene tutti i passaggi di marcatura in formato **.sqlite** assicurarsi che il valore ``MarkingReportSQLiteAppender`` sia attivo nella sezione **Marking Report Logger**:

.. code-block:: XML

   <!-- Marking Report Logger -->
   <logger name="SLC.Log.Report.ReportUtils" additivity="false">
      <!--<level value="ALL" /> <appender-ref ref="MarkingReportCSVFileAppender" />-->
      <appender-ref ref="MarkingReportSQLiteAppender" />
      <!--<appender-ref ref="MarkingReportSqlServerAppender" />-->
   </logger>


Il file **MarkingReportsDb.sqlite** contenente i dati è salvato nella cartella :guilabel:`Databases` (in genere *C:\\Sisma\\Databases)*).

.. _report_sqlserver:

IMPOSTAZIONI REPORT Microsoft SqlServer
---------------------------------------
Prisma può connettersi ad un'istanza di **Microsoft SqlServer** per salvare eventi e attività di marcatura.

Tabella SqlServer Report
^^^^^^^^^^^^^^^^^^^^^^^^
All'interno dell'istanza di **Microsoft SqlServer** creare un nuovo database e poi quindi creare una nuova tabella ``MarkingReports`` utilizzando la query:

.. code-block:: sql

   CREATE TABLE dbo.MarkingReports
   (
      [Date] datetime NOT NULL PRIMARY KEY,
      [MarkingEvent] nvarchar (100) NULL,
      [ProjectName] nvarchar (255) NULL,
      [WorkingPlaneName] nvarchar (100) NULL,
      [MultiPositionNum] nvarchar (100) NULL,
      [Level3D] int NULL,
      [Lasertime] int NULL,
      [AlarmType] int NULL,
      [CodeValidation] nvarchar (100) NULL,
      [PatternMatchedObjs] nvarchar (255) NULL,
      [DragToolFile] nvarchar (255) NULL,
      [Variables] nvarchar (255) NULL,
      [Layers] nvarchar (255) NULL,
      [Settings] nvarchar (255) NULL,
      [Cycletime] int NULL,
      [Position] nvarchar (100) NULL
   );

Connessione al database
^^^^^^^^^^^^^^^^^^^^^^^
Per salvare un file di registro che contiene tutti i passaggi di marcatura in formato **.sqlite** assicurarsi che il valore ``MarkingReportSqlServerAppender`` sia attivo nella sezione **Marking Report Logger**:

.. code-block:: XML

   <!-- Marking Report Logger -->
   <logger name="SLC.Log.Report.ReportUtils" additivity="false">
      <!--<level value="ALL" /> <appender-ref ref="MarkingReportCSVFileAppender" />-->
      <!--<appender-ref ref="MarkingReportSQLiteAppender" />-->
      <appender-ref ref="MarkingReportSqlServerAppender" />
   </logger>


All'interno della sezione ``Appender SqlServer`` di **Marking Report** utilizzare la stringa di connessione corretta per connettersi all'istanza **SqlServer**.

.. code-block:: XML

   <!-- Marking Report SqlServer appender -->
   <appender name="MarkingReportSqlServerAppender" type="log4net.Appender.AdoNetAppender">
      <bufferSize value="1"/>
      <connectionType value="System.Data.SqlClient.SqlConnection, System.Data, Version=1.0.3300.0, 
      Culture=neutral, PublicKeyToken=b77a5c561934e089"/>

      <!-- Remote connection -->
      <connectionString value="Server=SERVER\SQLEXPRESS;Database=ReportDb;User Id=user;Password=psw"/>
      <!-- Local connection -->
      <connectionString value="Server=PCNAME\SQLEXPRESS;Database=ReportDb;Trusted_Connection=True"/>
      
      <!-- ... -->
      
   </appender>


Scegliere uno tra **Remote connection** o **Local connection** in base alla connessione al server del database.

Compilare di conseguenza i valori:

* ``Server``
* ``Database``
* ``Id`` e ``Password`` (compilazione obbligatoria se si sceglie connessione remota).
