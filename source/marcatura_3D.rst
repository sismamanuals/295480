﻿#################
MARCATURA FILE 3D
#################
Il software PRISMA permette di caricare anche dei file 3D in formato **.txt** generati dal Plug-in di *Rhinoceros*.
Questi file 2D vengono generati dall'elaborazione di una matematica 3D, che viene poi convertita in layers per poter essere marcata dal laser.
Per marcare un file in 3D con PRISMA è necessario prima di tutto aver elaborato la matematica 3D con il Plug-in Sisma di Rhinoceros.
È poi indispensabile copiare la cartella in C:\RhinoSVG. Fatto questo si può caricare il file **.txt** nel programma PRISMA e procedere al settaggio del programma.

*Per maggiori chiarimenti relativamente al Plug-in Sisma si consiglia di fare riferimento al manuale fornito con il Plug-in stesso.*

*******************
SETTAGGIO PARAMETRI
*******************
La prima cosa da fare è settare i parametri di marcatura per il 3D.

Per ogni colore del File Livelli è necessario impostare opportuni parametri in modo da far corrispondere lo spessore del materiale asportato con il passo dei layer.
Se ciò non avviene, è facile riscontrare nella marcatura eseguita delle distorsioni, specialmente sulle forme piane.
È possibile individuare i parametri corretti eseguendo la marcatura di un cubetto, (ad es.4x4x1 mm) verificando che la profondità ottenuta coincida con quella del disegno.

I colori dei Livelli di marcatura sono identici per Potenza-Frequenza-Velocità-Spaziatura, differiscono solamente per l'angolo di riempimento.
Per l'angolo di riempimento, è bene fare in modo che il pezzo sia lavorato con più angoli, tra loro differenti.

La :ref:`marcatura_3D_tabella_colori_angoli` indica gli angoli opportuni per 8 o 9 colori utilizzati.

.. _marcatura_3D_tabella_colori_angoli:
.. figure:: _static/marcatura_3D_tabella_colori_angoli.png
   :width: 10 cm
   :align: center

   Tabella correlazione colori-angoli

Una volta trovata la parametrizzazione adeguata per il materiale che si andrà a incidere, è sufficiente salvare il :kbd:`File Livelli` e caricarlo nella memoria laser.

*******************
SETTAGGIO 3D PRISMA
*******************
Per poter usare il menù di gestione del file 3D è necessario importare il file **.txt** della cartella Struttura di Progetto. Per importare il file è sufficiente premere :guilabel:`Aggiungi file grafico` e aprire la tendina delle estensioni disponibili per selezionare 3D File Description (**.txt**).

Una volta aggiunto il file, nella finestra :guilabel:`Proprietà` verrà visualizzata la sezione 3D con i seguenti campi editabili:

* :kbd:`Corrente` (indica il fine che il laser sta per marcare - in alcuni casi potrebbe essere utile poter cambiare questo file)
* :kbd:`Usa Sollevatore` (di default il laser usa il movimento testa dell'asse Z che per la maggior parte delle macchine è di 0.01 mm. In alcuni casi in cui è richiesto uno step più piccolo

   * si può usare un Sollevatore opzionale che viene montato in macchina e comandato dall'elettronica del laser. In quel caso è necessario abilitare PRISMA per l'utilizzo di tale Sollevatore)

Sempre nella sezione 3D troviamo dei campi il cui significato è essenziale per capire e/o controllare l'attività del laser:

* :kbd:`Nome File` (mostra il percorso file dove il sistema andrà a prendere i layer da marcare)
* :kbd:`N Caratteri` (indica il numero di cifre che identifica il progressivo layer)
* :kbd:`Inizio` (indica il numero del primo layer svg che verrà marcato)
* :kbd:`Fine` (indica il numero dell'ultimo layer che verrà marcato)
* :kbd:`Passo Testa` (indica lo step che eseguirà la testa nella marcatura dei file)
* :kbd:`Passo File` (indica lo step di sezionamento che è stato usato con il Plug-in)

.. _marcatura_3D_impostazioni_3D:
.. figure:: _static/marcatura_3D_impostazioni_3D.png
   :width: 10 cm
   :align: center

   Tabella settaggi 3D

I campi appena descritti sono generati automaticamente dal Plug-in Sisma, ma possono essere modificati da un utente esperto editando con un Editor di testo il file **.txt** del 3D.
Per la modifica dello stesso si consiglia di fare riferimento al manuale del Plug-in Sisma.

************
MARCATURA 3D
************
Prima di procedere con la marcatura si ricorda che è possibile decide la posizione della marcatura in X e Y tramite gli appositi campi della sezione :guilabel:`File Grafico` della finestra :guilabel:`Proprietà`.

Per avviare la marcatura:

* Premere :kbd:`AVVIA ROSSO` per proiettare l'ingombro massimo all'interno della camera di lavoro. Tramite il rosso è possibile posizionare il pezzo in posizione desiderata.
* Chiudere la porta del laser.
* Premere :kbd:`AVVIA LASER` per eseguire il test.
