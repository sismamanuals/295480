##################################################
FUNZIONE DI CARICAMENTO PROGETTO DA CODICE A BARRE
##################################################
***********
DESCRIZIONE
***********
Il caricamento del progetto del codice a barre è una funzione di PRISMA che consente di caricare automaticamente un progetto leggendo un codice a barre. È possibile caricare anche alcuni valori su variabili con un solo codice a barre.

***************************
ABILITAZIONE DELLA FUNZIONE
***************************
Aprire :guilabel:`Strumenti > Impostazioni > Impostazioni applicazione`.
Selezionare il menù :guilabel:`Percorsi` e attivare il flag :guilabel:`Abilita Report` e scegliere il percorso del file di configurazione del progetto (vedere :numref:`caricamento_barcode_abilitazione`).

.. _caricamento_barcode_abilitazione:
.. figure:: _static/caricamento_barcode_abilitazione.png
   :width: 14 cm
   :align: center

   Abilitazione Caricamento da Codice a Barre

Selezionare il menù :guilabel:`Interfaccia` e attivare il flag :guilabel:`Read Project Code` all'interno della sezione *PROPRIETÀ GESTIONE PROGETTI* e scegliere il percorso del file di configurazione del progetto (vedere :numref:`caricamento_barcode_abilitazione`).
Selezionando il flag :guilabel:`Anteprima Rosso all'apertura del progetto` l'anteprima verrà prodotta subito dopo l'apertura per aiutare l'utente nell'operazione di centratura.

.. _caricamento_barcode_abilitazione_read_code:
.. figure:: _static/caricamento_barcode_abilitazione_read_code.png
   :width: 14 cm
   :align: center

   Abilitazione "Read Project Code"

Sull'interfaccia apparirà l'icona come in :numref:`caricamento_barcode_lettura_barcode`. Premendo questa icona si aprirà una finestra in attesa della lettura del codice. È anche possibile digitare manualmente codice.

.. _caricamento_barcode_lettura_barcode:
.. figure:: _static/caricamento_barcode_lettura_barcode.png
   :width: 14 cm
   :align: center

   Caricamento progetto da Codice a Barre

********************************
IMPOSTARE LA MASCHERA DEL CODICE
********************************
Sono possibili diverse opzioni per la maschera del codice:

* ``Vuoto``: il codice è solo il nome completo del progetto;
* ``*``: seleziona il carattere che identifica il nome del progetto;
* ``#``: seleziona il carattere da evitare;
* ``A .. Z``: per selezionare il carattere da associare a una variabile. È il nome della variabile (case sensitive);
* ``a .. z``: per selezionare il carattere da associare a una variabile. È il nome della variabile (case sensitive);
* ``?``: identifica l'ultimo carattere da leggere; il sistema ignorerà tutti i caratteri seguenti.

.. NOTE::

   |notice|
   
   * Se si utilizza la maschera di codice, la lunghezza del nome del progetto e il numero di caratteri per ciascuna variabile deve essere fissa e sempre nella stessa posizione.
   * Il nome delle variabili all'interno del progetto deve avere lo stesso tipo di formattazione maiuscolo o minuscolo (``A .. Z`` o ``a .. z``);
   * Nel progetto deve esserci un riferimento alla variabile C (``#[C]#``).

**Esempio:**
   *Codice letto*: 123AQ7P1234DFTY9745FTCV
   
   *Maschera del codice*: CCC####************?
   
   *Nome del progetto da caricare*: 1234DFTY9745
   
   *Valore della variabile C*: 123
