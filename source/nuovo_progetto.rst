##############
NUOVO PROGETTO
##############
Procedura di creazione nuovo progetto ed inserimento file.

******************
CREAZIONE PROGETTO
******************
Selezionare :guilabel:`File > Nuovo` oppure premere sulla relativa icona presente nella barra in alto.

.. _nuovo_progetto_lista_comandi:
.. figure:: _static/nuovo_progetto_lista_comandi.png
   :width: 10 cm
   :align: center

   Interfaccia nuovo progetto


Si apre un foglio quadrato che copre la massima estensione di marcatura possibile, in questa area possono essere depositati tutti gli elementi che si intende marcare.

.. _nuovo_progetto_pagina_bianca:
.. figure:: _static/nuovo_progetto_pagina_bianca.png
   :width: 14 cm
   :align: center

   Foglio


****************
INSERIMENTO FILE
****************
Selezionare :guilabel:`Progetto > Aggiungi File` oppure premere sulla relativa icona presente sulla barra.

Selezionare il file che si intende marcare nel percorso cartelle in cui si trova e premere :kbd:`Apri`.

I formati di file supportati sono di tipo:

* vettoriale (**.svg** - **.dxf** - **.dwg**);
* immagine (**.bmp** - **.jpg** - **.tiff** - **.png**);
* plotter (**.hpgl** - **.plt**);
* descrizione file 3D (**.txt**).

Una volta inserito, il file può subire una serie di operazioni. Queste operazioni si selezionano dalla barra presente sopra il foglio di lavoro.

OPERAZIONI SU FILE GRAFICO
==========================

.. _nuovo_progetto_operazioni_file_grafico:
.. figure:: _static/nuovo_progetto_operazioni_file_grafico.png
   :width: 10 cm
   :align: center

   Operazioni file grafico

Le operazioni che agiscono sul file sono (secondo l'ordine di disposizione sulla barra):

* :kbd:`Aggiungi File Grafico` (aggiunge un file grafico al progetto);
* :kbd:`Rimuovi File Grafico` (elimina un file grafico dal progetto);
* :kbd:`Taglia` (taglia un file grafico dal progetto);
* :kbd:`Copia` (copia un file grafico);
* :kbd:`Incolla` (incolla un file grafico nel progetto);
* :kbd:`Multi` (crea di una serie di copie del file grafico)
* :kbd:`Centra` (centra il file grafico rispetto all'area di lavoro);
* :kbd:`Specchia Orizzontalmente` (specchia il file in orizzontale);
* :kbd:`Specchia Verticalmente` (specchia il file in verticale);

.. _operazioni_livelli:

OPERAZIONI SU LIVELLI
=====================

.. _nuovo_progetto_operazioni_layer:
.. figure:: _static/nuovo_progetto_operazioni_layer.png
   :width: 10 cm
   :align: center

   Operazioni su livelli del file grafico


* :kbd:`Allinea Sopra`, :kbd:`Allinea Sotto`, :kbd:`Allinea in Centro`, :kbd:`Allinea a Sinistra`, :kbd:`Allinea a Destra`, :kbd:`Allinea Orizzontalmente` e :kbd:`Allinea Verticalmente` (permettono l'allineamento di più file sopra, sotto, al centro, a destra, a sinistra, al centro orizzontalmente o al centro verticalmente),
* :kbd:`Muovi Su` e :kbd:`Muovi Giù` (Cambio di livello di appartenenza del file più in alto o più in basso);
* :kbd:`Abilita` (abilitazione/disabilitazione visualizzazione file);
* :kbd:`Blocca` (blocco posizione);
* :kbd:`Modifica con programma esterno` (apertura file con editor esterno).

INSERIMENTO ELEMENTI
====================

.. _nuovo_progetto_operazioni_inserimento_elementi:
.. figure:: _static/nuovo_progetto_operazioni_inserimento_elementi.png
   :width: 14 cm
   :align: center

   Elementi aggiuntivi per file grafico


Oltre a file è possibile selezionare ed inserire i seguenti elementi (evidenziati in figura):

* :kbd:`Testo Lineare` (aggiunge testo);
* :kbd:`Testo Circolare` (aggiunge testo su curva);
* :kbd:`Text Editor` (apre finestra :kbd:`EDITOR TESTI` con parametrizzazioni dettagliate);
* :kbd:`Aggiungi Rettangolo` (aggiunge rettangolo solo bordo);
* :kbd:`Aggiungi Cerchio` (aggiunge cerchio solo bordo);
* :kbd:`Aggiungi Rettangolo` (aggiunge rettangolo pieno);
* :kbd:`Aggiungi Cerchio` (aggiunge cerchio pieno);
* :kbd:`Aggiungi croce` (aggiunge una croce);
* :kbd:`Aggiungi Barcode` (aggiunge un codice a barre) - vedere :numref:`barcode` :ref:`barcode`;
* :kbd:`Aggiungi Codice 2D` (aggiunge un codice 2D) - vedere :numref:`datamatrix` :ref:`datamatrix`.

Una volta selezionato l'elemento desiderato si possono definirne le caratteristiche selezionando le varie opzioni disponibili nella finestra :guilabel:`Proprietà`.

Al momento della selezione dell'oggetto, ai bordi appariranno dei punti di controllo che permettono di cambiare dimensioni e proporzioni del file e di ruotare l'elemento.

Tra i punti di controllo presenti, il punto di colore grigio rappresenta il punto principale di controllo a cui si riferiscono anche le coordinate.

.. _nuovo_progetto_operazioni_punti_controllo:
.. figure:: _static/nuovo_progetto_operazioni_punti_controllo.png
   :width: 10 cm
   :align: center

   Punti di controllo

.. _salvataggio_impostazioni:

************************
SALVATAGGIO IMPOSTAZIONI
************************

.. NOTE::

   |notice| A questo punto si consiglia di salvare il nuovo progetto creato, prima di proseguire con l'impostazione delle caratteristiche e dei parametri di marcatura.
   Per fare questo selezionare :guilabel:`File > Salva` o premere sull'icona delle barra comandi |salva_file| ed assegnare un percorso ed un nome al nuovo file.
