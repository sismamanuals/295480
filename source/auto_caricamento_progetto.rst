###############################
AUTO CARICAMENTO PROGETTI (ACP)
###############################

Il caricamento automatico dei progetti è una funzione Prisma che consente di caricare automaticamente un elenco di progetti da contrassegnare, usando un semplice file **.XML**.

***************************
ABILITAZIONE DELLA FUNZIONE
***************************

Aprire :guilabel:`Strumenti > Impostazioni > Impostazioni applicazione`.
Selezionare il menù :guilabel:`Percorsi` e attivare il flag :guilabel:`Auto caricamento progetto` e scegliere il percorso del file di configurazione del progetto (vedere :numref:`auto_caricamento_progetto_abilitazione`.

.. _auto_caricamento_progetto_abilitazione:
.. figure:: _static/auto_caricamento_progetto_abilitazione.png
   :width: 14 cm
   :align: center

   Abilitazione Caricamento automatico Progetto

*****************************************
CREAZIONE FILE DI CONFIGURAZIONE PROGETTO
*****************************************
Creare un nuovo file **XML** contenente tutti i progetti da elaborare, usando questo formato:

.. code-block:: XML

   <!-- XML Header -->
   <?xml version="1.0" encoding="utf-8"?>
   <AutomaticProjectLoad xmlns:xsd="http://www.w3.org/2001/XMLSchema"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

      <!-- Projects list -->
          <Projects>

            <!-- First project -->
            <Project>

               <!-- Project path -->
               <Path>C:\test\projects\project1.slcz</Path>

               <!-- Copies -->
               <Copies>3</Path>

               <!-- Counters (key, value) -->
               <Counters>

                  <CounterItem>
                     <Key>aa</Key>   <!-- First counter -->
                     <Value>125</Value>
                  </CounterItem>

                  <CounterItem>
                     <Key>bb</Key>   <!-- Second counter -->
                     <Value>aa11bba</Value>
                  </CounterItem>

               </Counters>

               <!-- Working planes -->
               <WorkingPlanes>
                  <WorkingPlane>
                     <Index>1</Index>   <!-- Int 1 based index -->
                     <X>100.00</X>
                     <Y>100.00</Y>
                     <Z>100.00</Z>
                     <Enabled>1</Enabled> <!-- 0 disabled e 1 enabled -->
                  </WorkingPlane>
                  <WorkingPlane>
                  <Index>2</Index>
                     <Y>150.00</Y>	<!-- Write only changes quote -->
                     <Enabled>1</Enabled>
                  </WorkingPlane>
               </WorkingPlanes>

               <!-- Start command: 0 no cmd, 1 red cmd, 2 mark cmd -->
               <StartCmd>1</StartCmd>
            </Project>

               <!-- Follow second project -->
            <Project>
               <Path>C:\test\projects\project2.slcz</Path>
               <Counters>
                  <CounterItem>
                  <Key>var1</Key>
                  <Value>27</Value>
               </CounterItem>
            </Counters>
            <StartCmd>1</StartCmd>
         </Project>

      </Projects>
   </AutomaticProjectLoad>

Il tag *<Projects>* contiene tutti gli elementi del progetto da elaborare automaticamente.
Ogni progetto contiene:

* *<Path>*: il percorso del file di progetto;
* *<Copies>*: copie del progetto;
* *<Counters>*: l'elenco di CounterItem con una coppia chiave / valore;
* *<WorkingPlanes>*: elenco di piani di lavoro con indice, abilitazione e posizione
* *<StartCmd>*: comando da eseguire dopo l'apertura del progetto (0 = niente, 1 = comando rosso, 2 = comando mark).

*************************************
AVVIO CARICAMENTO AUTOMATICO PROGETTO
*************************************
Per avviare la funzione di Caricamento automatico è sufficiente premere il pulsante nel menu principale, come indicato in :numref:`auto_caricamento_progetto_avvio`.

.. _auto_caricamento_progetto_avvio:
.. figure:: _static/auto_caricamento_progetto_avvio.png
   :width: 10 cm
   :align: center

   Caricamento automatico Progetto

Prisma leggerà il file di configurazione XML specificato in :guilabel:`File di configurazione progetto`, caricando tutti i componenti del progetto in memoria e inizierà l'elaborazione del primo. PRISMA eseguirà il backup del file di configurazione in una cartella denominata *backup* e aggiungerà informazioni sulla data/ora al nome del file.
Infine mostrerà una finestra sopra l'interfaccia principale (:numref:`auto_caricamento_progetto_interazione`), che consente di interagire usando alcuni pulsanti di comando (:kbd:`Luce`, :kbd:`Rosso`, :kbd:`Avvio`, :kbd:`Stop`, :kbd:`Annulla`).

Durante il periodo in cui la finestra è aperta, non è possibile modificare progetti o fare altri operazioni.
La pressione del pulsante :kbd:`Annulla` interromperà la funzione di ACP e tornerà alla modalità PRISMA standard.

.. _auto_caricamento_progetto_interazione:
.. figure:: _static/auto_caricamento_progetto_interazione.png
   :width: 14 cm
   :align: center

   Finestra di interazione con processo
