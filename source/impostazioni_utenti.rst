.. _impostazioni_utenti:

########################
IMPOSTAZIONI MENÙ UTENTI
########################

.. |Abilita_Gestione_Utenti| image:: _static/impostazioni_utenti_abilita.png
   :width: 1 cm
   :align: middle

.. |Aggiungi_Utente| image:: _static/impostazioni_utenti_aggiungi.png
   :width: 1 cm
   :align: middle

.. |Elimina_Utente| image:: _static/impostazioni_utenti_elimina.png
   :width: 1 cm
   :align: middle

.. _impostazioni_applicazione_utenti:
.. figure:: _static/impostazioni_applicazione_utenti.png
   :width: 14 cm
   :align: center

   Menù Utenti


Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > Utenti` permette di aggiungere e togliere utenti e di definirne le abilitazioni.

* Il pulsante |Abilita_Gestione_Utenti| permette di abilitare la gestione degli utenti;
* Il pulsante |Aggiungi_Utente| permette di aggiungere un utente;
* Il pulsante |Elimina_Utente| permette di eliminare un utente.

Per ogni utente si deve definire il ruolo **Administrator** oppure **Basic User** e definire :kbd:`Nome` e :kbd:`Password`.

******************
PROPRIETÀ GENERALI
******************
**Acconsenti disabilitazione Piani Lavoro**

   *Inserire descrizione*

**Settaggi**

   *Inserire descrizione*

**Livelli**

   *Inserire descrizione*

**Modalità**

   *Inserire descrizione*

**Posizione**

   *Inserire descrizione*

**Variabili e Foglio dati**

   *Inserire descrizione*

**Allarmi**

   *Inserire descrizione*

**Copie multiple**

   *Inserire descrizione*

**Lavoro in Pausa**

   *Inserire descrizione*

**Messaggio personalizzato**

   *Inserire descrizione*

**Posizione personalizzata**

   *Inserire descrizione*
